# Rusty button

Button that only passes circuit closures from input pin to the output pin after a password has been entered.

Used as a protection against felines.

Generated from https://github.com/daschl/nrf52840dk-sample

Run using https://github.com/knurling-rs/probe-run
