#![no_main]
#![no_std]

use cortex_m::prelude::_embedded_hal_blocking_delay_DelayMs;
use defmt_rtt as _;
use hal::Timer;
use nrf52840_hal as hal; // memory layout
use panic_probe as _;

use hal::gpio::Level;
use hal::pac::Peripherals;
use hal::prelude::*;

#[derive(Debug)]
enum State {
    Released(u16),
    Pressed(u16),
}

const POLL_INTERVAL_MS: u16 = 10;
const RESET_THRESHOLD_MS: u16 = 1000;

/// The press after 1 second after the password is entered is transfered to the output
static PASSWORD: &[u16] = &[500, 100, 100];
static PRECISION_MS: u16 = 200;

#[cortex_m_rt::entry]
fn main() -> ! {
    defmt::info!("Hello, World!");

    let p = Peripherals::take().unwrap();
    let port = hal::gpio::p1::Parts::new(p.P1);
    let mut led = port.p1_11.into_push_pull_output(Level::High);
    let button = port.p1_10.into_pullup_input();
    let mut timer = Timer::periodic(p.TIMER0);

    use State::*;

    let mut state = Released(0);
    let mut password_progress = 0;
    let mut attempt_failed = false;

    loop {
        let is_button_pressed = button.is_low().unwrap();
        if is_button_pressed {
            if password_progress == PASSWORD.len() {
                led.set_low().unwrap();
            }
            match state {
                Pressed(ref mut p) => {
                    *p = p.saturating_add(POLL_INTERVAL_MS);
                }
                Released(..) => state = Pressed(0),
            }
        } else {
            led.set_high().unwrap();
            match state {
                Pressed(d) => {
                    state = Released(0);
                    if attempt_failed {
                        defmt::info!("Ignoring, waiting reset");
                    } else if password_progress < PASSWORD.len() {
                        defmt::info!("Expected {}", PASSWORD[password_progress]);
                        let current_pw_interval = PASSWORD[password_progress]
                            .saturating_sub(PRECISION_MS)
                            ..(PASSWORD[password_progress] + PRECISION_MS);
                        if current_pw_interval.contains(&(d as u16)) {
                            password_progress += 1;
                            defmt::info!("Progress: {}", d);
                        } else {
                            defmt::info!("Fail: {}", d);
                            attempt_failed = true;
                        }
                    }
                }
                Released(ref mut d) => {
                    *d = d.saturating_add(POLL_INTERVAL_MS);
                    if *d >= RESET_THRESHOLD_MS {
                        if attempt_failed || password_progress != 0 {
                            defmt::info!("Reset");
                        }
                        password_progress = 0;
                        attempt_failed = false;
                        
                    }
                }
            }
        }
        timer.delay_ms(POLL_INTERVAL_MS);
    }
}

pub fn exit() -> ! {
    loop {
        cortex_m::asm::bkpt();
    }
}
